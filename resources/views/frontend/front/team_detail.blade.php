@extends('layouts.frontend')
@section('content')
    <!-- about -->
    <div class="about">

        <div class="container">

            <h2 class="heading-agileinfo">{{$data['team']->name}}</h2>
            @include('includes.flash')
            <div class="about-grids-1">
                <div class="col-md-5 wthree-about-left">
                    <div class="wthree-about-left-info">
                        <img src='{{asset('images/team/' . $data['team']->image)}}' class="img-responsive" data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="team-image1" />
                    </div>
                </div>
                <div class="col-md-7 agileits-about-right">
                    <h5>{{$data['team']->title}}</h5>
                </div>
                {{--<span>{!!  $data['team']->description!!}</span>--}}
                <div class="clearfix"> </div>
            </div>

        </div>

    </div>
@endsection