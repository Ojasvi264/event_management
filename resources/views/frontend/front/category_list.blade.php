@extends('layouts.frontend')
<!--Events -->
@section('content')
    <div class="ser_agile">
        <div class="container">
            {{--<h2 class="heading-agileinfo">{{$data['welcome'][0]->name}}<span>{{$data['welcome'][0]->title}}</span></h2>--}}
            {{--<p>{!! $data['welcome'][0]->description!!}</p>--}}
            <div class="ser_w3l">
                @foreach($data['categories'] as $category)
                    <div class="outer-wrapper">
                        <div class="inner-wrapper" >
                            <div class="icon-wrapper">
                                <img src='{{asset('images/category/' . $category->image)}}'  data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="category-icon1" />
                            </div>
                            <div class="content-wrapper">
                                <h4><a href="{{route('frontend.category',$category->id)}}" >{{$category->name}}</a></h4>
                                <p>{{$category->meta_description}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
            <div class="pages text-right">
                {{$data['categories']->links()}}
            </div>
        </div>
    </div>
    <!-- //Events -->


@endsection

