@extends('layouts.frontend')
<!--Events -->
@section('content')
    <div class="events-agileits-w3layouts">
        <div class="container">
            <h3 class="heading-agileinfo">Event Manager Says<span>Events is a professionally managed Event</span></h3>
            <div class="popular-grids">
                @foreach($data['testimonials'] as $testimonial)
                    <div class="col-md-4 popular-grid">
                        <img src='{{asset('images/testimonial/' . $testimonial->image)}}' height="350" data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="testimonial-image1" />
                        <div class="popular-text">
                            <h5><a href="{{route('frontend.testimonial_detail',$testimonial->id)}}" >{{$testimonial->name}}</a></h5>
                            <div class="detail-bottom">
                                <ul>
                                    <li><i class="fa fa-calendar" aria-hidden="true"></i>{{$testimonial->designation}}</li>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{$testimonial->title}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
            <div class="pages text-right">
                {{$data['testimonials']->links()}}
            </div>
        </div>
    </div>
    <!-- //Events -->


@endsection

