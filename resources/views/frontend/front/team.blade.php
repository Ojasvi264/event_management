@extends('layouts.frontend')
<!--Events -->
@section('content')
    <div class="events-agileits-w3layouts">
        <div class="container">
            <h3 class="heading-agileinfo">Event Manager Says<span>Events is a professionally managed Event</span></h3>
            <div class="popular-grids">
                @foreach($data['teams'] as $team)
                    <div class="col-md-4 popular-grid">
                        <img src='{{asset('images/team/' . $team->image)}}' height="300" data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="team-image1" />
                        <div class="popular-text">
                            <h5><a href="{{route('frontend.team_detail',$team->id)}}" >{{$team->name}}</a></h5>
                            <div class="detail-bottom">
                                <ul>
                                    {{--<li><i class="fa fa-calendar" aria-hidden="true"></i>{{$team->designation}}</li>--}}
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{$team->title}}</li>
                                </ul>
                                {{--<p>{{$team->description}}</p>--}}
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
            <div class="pages text-right">
                {{$data['teams']->links()}}
            </div>
        </div>
    </div>
    <!-- //Events -->


@endsection

