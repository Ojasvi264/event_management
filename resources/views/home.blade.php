@extends('layouts.backend')
@section('title','Dashboard page')
@section('js')
    <script>$('#my-todo-list').todoList({
            onCheck: function(checkbox) {
            },
            onUnCheck: function(checkbox) {
            }
        })</script>
    <script>
        mobiscroll.setOptions({
            theme: 'ios',
            themeVariant: 'light',
            dragToCreate: true,
            dragToMove: true,
            dragToResize: true
        });

        var calendar,
            popup,
            range,
            oldEvent,
            showArrow,
            tempID = 5,
            tempEvent = {},
            deleteEvent,
            restoreEvent,
            titleInput = document.getElementById('event-title'),
            descriptionTextarea = document.getElementById('event-desc'),
            allDaySwitch = document.getElementById('event-all-day'),
            freeSegmented = document.getElementById('event-status-free'),
            busySegmented = document.getElementById('event-status-busy'),
            deleteButton = document.getElementById('event-delete'),
            now = new Date(),
            myData = [{
                id: 1,
                start: new Date(now.getFullYear(), now.getMonth(), 8, 13),
                end: new Date(now.getFullYear(), now.getMonth(), 8, 13, 30),
                title: 'Lunch @ Butcher\'s',
                color: '#26c57d'
            }, {
                id: 2,
                start: new Date(now.getFullYear(), now.getMonth(), now.getDate(), 15),
                end: new Date(now.getFullYear(), now.getMonth(), now.getDate(), 16),
                title: 'General orientation',
                color: '#fd966a'
            }, {
                id: 3,
                start: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1, 18),
                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1, 22),
                title: 'Dexter BD',
                color: '#37bbe4'
            }, {
                id: 4,
                start: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, 10, 30),
                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, 11, 30),
                title: 'Stakeholder mtg.',
                color: '#d00f0f'
            }];

        function createAddPopup(elm) {
            // hide delete button inside add popup
            deleteButton.style.display = 'none';

            deleteEvent = true;
            restoreEvent = false;

            // set popup header text and buttons for adding
            popup.setOptions({
                headerText: 'New event',
                buttons: [
                    'cancel',
                    {
                        text: 'Add',
                        keyCode: 'enter',
                        handler: function () {
                            calendar.updateEvent(tempEvent);
                            deleteEvent = false;
                            popup.close();
                        },
                        cssClass: 'mbsc-popup-button-primary'
                    }
                ]
            });

            // fill popup with a new event data
            mobiscroll.getInst(titleInput).value = tempEvent.title;
            mobiscroll.getInst(descriptionTextarea).value = '';
            mobiscroll.getInst(allDaySwitch).checked = false;
            range.setVal([tempEvent.start, tempEvent.end]);
            mobiscroll.getInst(busySegmented).checked = true;

            // set anchor for the popup
            popup.setOptions({ anchor: elm, showArrow: true });

            popup.open();
            // show popup arrow
            showArrow = true;
        }

        function createEditPopup(args) {
            var ev = args.event;
            // show delete button inside edit popup
            deleteButton.style.display = 'block';

            deleteEvent = false;
            restoreEvent = true;

            // set popup header text and buttons for editing
            popup.setOptions({
                headerText: 'Edit event',
                buttons: [
                    'cancel',
                    {
                        text: 'Save',
                        keyCode: 'enter',
                        handler: function () {
                            var date = range.getVal();
                            // update event with the new properties on save button click
                            calendar.updateEvent({
                                id: ev.id,
                                title: titleInput.value,
                                description: descriptionTextarea.value,
                                allDay: mobiscroll.getInst(allDaySwitch).checked,
                                start: date[0],
                                end: date[1],
                                free: mobiscroll.getInst(freeSegmented).checked,
                                color: ev.color,
                            });
                            restoreEvent = false;
                            popup.close();
                        },
                        cssClass: 'mbsc-popup-button-primary'
                    }
                ]
            });

            // fill popup with the selected event data
            mobiscroll.getInst(titleInput).value = ev.title || '';
            mobiscroll.getInst(descriptionTextarea).value = ev.description || '';
            mobiscroll.getInst(allDaySwitch).checked = ev.allDay || false;;
            range.setVal([ev.start, ev.end]);

            if (ev.free) {
                mobiscroll.getInst(freeSegmented).checked = true;
            } else {
                mobiscroll.getInst(busySegmented).checked = true;
            }

            // change range settings based on the allDay
            calendar.setOptions({ controls: ev.allDay ? ['calendar'] : ['calendar', 'time'] });

            // set anchor for the popup
            popup.setOptions({ anchor: args.domEvent.currentTarget, showArrow: true });
            popup.open();
            // we want to show the popup's arrow at this point
            showArrow = true;
        }

        function positionPopup() {
            // show or hide popup arrow
            if (showArrow) {
                showArrow = false;
            } else {
                popup.setOptions({ showArrow: false });
            }
        }

        calendar = mobiscroll.eventcalendar('#demo-add-delete-event', {
            view: {
                calendar: { labels: true }
            },
            data: myData,
            dragToCreate: true,
            dragToMove: true,
            dragToResize: true,
            onEventClick: function (args) {
                oldEvent = { ...args.event };
                tempEvent = args.event;

                if (!popup.isVisible()) {
                    createEditPopup(args);
                }
            },
            onEventCreated: function (args) {
                popup.close();
                // store temporary event
                tempEvent = args.event;
                createAddPopup(args.target);
            },
            onEventDeleted: function () {
                mobiscroll.toast({
                    message: 'Event deleted'
                });
            }
        });

        popup = mobiscroll.popup('#demo-add-popup', {
            width: 400,
            // showOverlay: false,
            display: 'bottom',
            fullScreen: true,
            contentPadding: false,
            cssClass: 'crud-popup',
            onClose: function () {
                if (deleteEvent) {
                    calendar.removeEvent(tempEvent);
                } else if (restoreEvent) {
                    calendar.updateEvent(oldEvent);
                }
            },
            responsive: {
                medium: {
                    display: 'anchored',
                    width: 400,
                    fullScreen: false,
                    touchUi: false
                }
            }
        });

        titleInput.addEventListener('input', function (ev) {
            // update current event's title
            tempEvent.title = ev.target.value;
            // update current event in calendar
            calendar.updateEvent(tempEvent);
        });

        descriptionTextarea.addEventListener('change', function (ev) {
            // update current event's title
            tempEvent.description = ev.target.value;
        });

        allDaySwitch.addEventListener('change', function () {
            var checked = this.checked
            // change range settings based on the allDay
            range.setOptions({ controls: checked ? ['calendar'] : ['calendar', 'time'] });

            // update current event's allDay property
            tempEvent.allDay = checked;

            // update current event in calendar
            calendar.updateEvent(tempEvent);

            showArrow = false;

            positionPopup();
        });

        range = mobiscroll.datepicker('#event-date', {
            controls: ['calendar', 'time'],
            select: 'range',
            startInput: '#start-input',
            endInput: '#end-input',
            showRangeLabels: false,
            display: 'bubble',
            touchUi: false,
            dateWheels: '|DDD MMM D|',
            mode: 'datetime',
            onChange: function (args) {
                var date = args.value;
                // update event's start date
                tempEvent.start = date[0];
                tempEvent.end = date[1];

                // update current event in calendar
                calendar.updateEvent(tempEvent);

                // navigate the calendar to the correct view
                calendar.navigate(date[0]);

                showArrow = false;

                positionPopup();
            }
        });

        document.querySelectorAll('input[name=event-status]').forEach(function (elm) {
            elm.addEventListener('change', function () {
                // update current event's free property
                tempEvent.free = mobiscroll.getInst(freeSegmented).checked;
            });
        });

        deleteButton.addEventListener('click', function () {
            // delete current event on button click
            calendar.removeEvent(oldEvent);
            popup.close();

            mobiscroll.toast({
                message: 'Event deleted'
            });
        });
    </script>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard page
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        {{--<div class="box">--}}
            {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">Title</h3>--}}

                {{--<div class="box-tools pull-right">--}}
                    {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"--}}
                            {{--title="Collapse">--}}
                        {{--<i class="fa fa-minus"></i></button>--}}
                    {{--<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">--}}
                        {{--<i class="fa fa-times"></i></button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="box-body">--}}
                {{--Start creating your amazing application!--}}
            {{--</div>--}}
            {{--<!-- /.box-body -->--}}
            {{--<div class="box-footer">--}}
                {{--Footer--}}
            {{--</div>--}}
            {{--<!-- /.box-footer-->--}}
        {{--</div>--}}
        <!-- /.box -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$categories}}</h3>
                        <p>Total Categories</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{route('category.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$events}}<sup style="font-size: 20px"></sup></h3>

                        <p>Total Events</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{route('event.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$users}}</h3>

                        <p>Total Users</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{route('user.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$services}}</h3>

                        <p>Total Services</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="{{route('service.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <div class="row">
    <section class="col-lg-6 connectedSortable">
        <!-- TO DO List -->
        <div class="box box-primary">
            <div class="box-header">
                <i class="ion ion-clipboard"></i>

                <h3 class="box-title">To Do List</h3>
            </div>
             @include('includes.flash')
            <!-- /.box-header -->
            <div class="box-body">
                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                <ul class="todo-list" data-widget="todo-list">
                    @foreach ($tasks as $storedTask)
                        <li>
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                                <!-- checkbox -->
                            <input type="checkbox" value=""/>
                                <!-- todo text -->
                            <span class="text">{{ $storedTask->name }}</span>
                            <div class="tools">
                                <a href="#"><i class="fa fa-edit"></i></a>
                                    <form action="{{route('home.destroy',$storedTask->id)}}" method="post"
                                        onsubmit="return confirm('Are you sure?')">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE"/>
                                        <button><i class="fa fa-trash"></i></button>
                                    </form>
                            </div>
                        </li>
                    @endforeach
                </ul>
                    <div class="box-tools pull-right">
                        <ul class="pagination pagination-sm inline">
                            {{ $tasks->links() }}
                        </ul>
                    </div>
            </div>
            <div class="box-footer clearfix no-border">
                <form action="{{ route('home.store') }}" method="post">
                {{csrf_field()}}
                <input type="text" name="newTaskName" class="form-control" required>
                <button type="submit" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </form>
            </div>
        </div>
    </section>

    <section class="col-lg-6 connectedSortable">
        <div class="box box-solid bg-green-gradient" >
            <div id="demo-add-delete-event"></div>

            <div id="demo-add-popup">
                <div class="mbsc-form-group">
                    <label>
                        Title
                        <input mbsc-input id="event-title">
                    </label>
                    <label>
                        Description
                        <textarea mbsc-textarea id="event-desc"></textarea>
                    </label>
                </div>
                <div class="mbsc-form-group">
                    <label for="event-all-day">
                        All-day
                        <input mbsc-switch id="event-all-day" type="checkbox" />
                    </label>
                    <label for="start-input">
                        Starts
                        <input mbsc-input id="start-input" />
                    </label>
                    <label for="end-input">
                        Ends
                        <input mbsc-input id="end-input" />
                    </label>
                    <div id="event-date"></div>
                    <label>
                        Show as busy
                        <input id="event-status-busy" mbsc-segmented type="radio" name="event-status" value="busy">
                    </label>
                    <label>
                        Show as free
                        <input id="event-status-free" mbsc-segmented type="radio" name="event-status" value="free">
                    </label>
                    <div class="mbsc-button-group">
                        <button class="mbsc-button-block" id="event-delete" mbsc-button data-color="danger" data-variant="outline">Delete event</button>
                    </div>
                </div>
            </div>
        </div>




            </section>
        </div>
    </section>
    <!-- /.content -->
@endsection
