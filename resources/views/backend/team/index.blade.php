@extends('layouts.backend')
@section('title','Team index page')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Team Management
            <a href="{{route('team.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('team.create')}}">Team</a></li>
            <li class="active">Index page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Index Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered" id="datatable">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Designation</th>
                        {{--<th>fb_link</th>--}}
                        {{--<th>twitter_link</th>--}}
                        {{--<th>linkedin_link</th>--}}
                        {{--<th>Action</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data['teams'] as $team)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$team->name}}</td>
                            <td>{{$team->title}}</td>
                            <td> <div class="img-container">
                                    <a href="{{asset('images/team/' .$team->image)}}" data-lightbox="image-1" data-title="{{$team->image}}">
                                    <img src="{{asset('images/team/' .$team->image)}}" alt="" height="100" width="100">
                                </div></td>
                            <td>{{$team->designation}}</td>
                            {{--<td>{{$team->fb_link}}</td>--}}
                            {{--<td>{{$team->twitter_link}}</td>--}}
                            {{--<td>{{$team->linkedin_link}}</td>--}}
                            <td>
                                <a href="{{route('team.show',$team->id)}}" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    View
                                </a>
                                <a href="{{route('team.edit',$team->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                <form action="{{route('team.destroy',$team->id)}}" method="post"
                                      onsubmit="return confirm('Are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>


                                    <a href="{{asset('images/team/' . $team->image)}}" class="btn btn-primary">
                                        <i class="fa fa-info-circle"></i>
                                        Detail
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        } );
    </script>
@endsection