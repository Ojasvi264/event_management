@extends('layouts.backend')
@section('title','Service index page')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Service Management
            <a href="{{route('service.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('service.create')}}">Service</a></li>
            <li class="active">Index page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Index Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered" id="datatable">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Category Name</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data['services'] as $service)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$service->category->name}}</td>
                            <td>{{$service->name}}</td>
                            <td>
                                <div class="img-container">
                                    <a href="{{asset('images/service/' .$service->image)}}" data-lightbox="image-1" data-title="{{$service->image}}">
                                    <img src="{{asset('images/service/' .$service->image)}}" alt="" height="100" width="100">
                                    </a>
                                </div>
                            </td>
                            <td>{{$service->title}}</td>
                            <td>
                                <a href="{{route('service.show',$service->id)}}" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    View
                                </a>
                                <a href="{{route('service.edit',$service->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                <form action="{{route('service.destroy',$service->id)}}" method="post"
                                      onsubmit="return confirm('Are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>

                                    <a href="{{route('frontend.service_detail',$service->id)}}" class="btn btn-primary">
                                        <i class="fa fa-info-circle"></i>
                                        Detail
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        } );
    </script>
@endsection