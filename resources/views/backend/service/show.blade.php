@extends('layouts.backend')
@section('title','Service View page')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Service Management
            <a href="{{route('service.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
            <a href="{{route('service.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('service.index')}}">Service</a></li>
            <li class="active">View page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <td>{{$data['service']->category->name}}</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td>{{$data['service']->name}}</td>
                    </tr>
                    <tr>
                        <th>Image</th>
                        <td>
                            <div class="img-container">
                                <img src="{{asset('images/service/' .$data['service']->image)}}" alt="" height="100" width="100">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>{!! $data['service']->description !!}</td>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <td>{{$data['service']->title}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            @if($data['service']->status==1)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">InActive</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>{{$data['service']->created_at->format('j F,Y')}}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$data['service']->updated_at->format('j F,Y')}}</td>
                    </tr>
                    <tr>
                        <th>Deleted At</th>
                        <td></td>
                    </tr>
                    <tr>
                            <td>
                                <a href="{{route('service.edit',$data['service']->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                            </td>
                        <td>
                                <form action="{{route('service.destroy',$data['service']->id)}}" method="post"
                                      onsubmit="return confirm('Are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                            </td>
                    </tr>
                    </thead>
                </table>

                {{--<div class="row">--}}
                    {{--{{$data['category']->image}}--}}
                    {{--<div class="col-md-3">--}}
                        {{--<div class="img-container">--}}
                            {{--<img src="{{asset('images/service/' .$data['service']->image)}}" alt="" height="100" width="100">--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection