<div class="form-group">
    {!!  Form::label('role_id', 'Role Name'); !!}
    {!! Form::select('role_id', $data['roles'],null,['class' => 'form-control','placeholder' => 'Please Select Role']); !!}
</div>
<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>
<div class="form-group">
    {!!  Form::label('email', 'Email'); !!}
    {!! Form::email('email', null,['class' => 'form-control','id' => 'email']); !!}
    @include('includes.single_field_validation',['field'=>'email'])
</div>
<div class="form-group">
    {!!  Form::label('password', 'Password'); !!} <br>
    {!! Form::password('password', null,['class' => 'form-control','id' => 'password']); !!}
    @include('includes.single_field_validation',['field'=>'password'])
</div>
<div class="form-group">
    {!!  Form::label('phone', 'Phone Number'); !!}
    {!! Form::number('phone', null,['class' => 'form-control','id' => 'phone']); !!}

    @include('includes.single_field_validation',['field'=>'phone'])
</div>

<div class="form-group">
    {!!  Form::label('address', 'Address'); !!}
    {!! Form::text('address', null,['class' => 'form-control','id' => 'address']); !!}
    @include('includes.single_field_validation',['field'=>'address'])
</div>
<div class="form-group">
    <i class="fa fa-picture-o"></i>
    {!!  Form::label('photo', 'Image'); !!}
    {!! Form::file('photo', null,['class' => 'form-control','id' => 'photo']); !!}

    {{--{!! Form::image('','success', ['id'=>'image'], array( 'width' => 32, 'height' => 32 ))  !!}--}}

    @if(isset($data['user']) && $data['user']->image)
        <img id="image" src="{{asset('images/user/' . $data['user']->image)}}" width="100" height="100">
        {!! Form::hidden('photo', $data['user']->image); !!}
    @endif

    @include('includes.single_field_validation',['field'=>'photo'])
</div>

<div class="form-group">
    {!!  Form::label('description', 'Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control','id' => 'description']); !!}

    @include('includes.single_field_validation',['field'=>'description'])
</div>
