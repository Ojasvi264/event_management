<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Event;
use App\Model\Service;
use App\Model\User;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        return view('home');
        $events =Event::where('status', 1)->count();
//        $past_events =Event::where('date', '<=', Carbon::now()->toDateString())->count();
//        $upcomming_events =Event::where('date', '>', Carbon::now()->toDateString())->count();
        $users = User::orderBy('Created_by', 'desc')->count();
        $categories =Category::where('status', 1)->count();
        $services =Service::orderBy('created_by', 'desc')->count();
        $tasks = Task::orderBy('id','desc')->paginate(5);
        return view('home', compact('events','tasks', 'categories', 'services', 'users'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'newTaskName' => 'required|min:5|max:255',
        ]);
        $task = new Task;
        $task->name = $request->newTaskName;
        $task->save();
        if ($task) {
            $request->session()->flash('success_message', 'New task has been succesfully added!');
        }else{
            $request->session()->flash('error_message','New task failed to add');
        }
//        Session::flash('success', 'New task has been succesfully added!');
        return redirect()->route('home');
    }
    public function edit($id)
    {
        $task = Task::find($id);
        return view('home')->with('taskUnderEdit', $task);
    }
    public function destroy(Request $request, $id)
    {
        $task = Task::find($id);
//        $task->delete();
        if ($task->delete()) {
            $request->session()->flash('success_message', 'Task has been deleted successfully!');
//            Session::flash('success', 'Task #' . $id . ' has been successfully deleted.');
        }else{
            $request->session()->flash('error_message','Task deletion failed!');
        }
        return redirect()->route('home');
    }

}
