<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\UserRequest;
use App\Model\Role;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function __construct()
////    {
////        $this->middleware('admin');
////    }
    public function index()
    {
        $data['users']=User::all();
        $data['roles']=Role::all();
        return view('backend.user.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['roles'] = Role::pluck('name','id');
        //$data['roles']=Role::all();
        return view('backend.user.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $request->request->add(['created_by'=>Auth::user()->id]);
        if (!empty($request->has('photo'))) {
            //dd($request->all());
            $user_image = $request->file('photo');

            $image_name = uniqid() . '.' . $user_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/user');
            $user_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }
        $user=User::create($request->all());

        if ($user) {
            $request->session()->flash('success_message', 'User created Successfully');
            return redirect()->route('user.index');

        }else{
            $request->session()->flash('error_message','User created Failed');
            return redirect()->route('user.create');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['user']=User::find($id);
        $data['users']=User::where('id', $id)->get();
        return view('backend.user.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['roles'] = Role::pluck('name','id');
        // $data['roles']=Role::all();
        $data['user']=User::find($id);
        return view('backend.user.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $request->request->add(['created_by'=>Auth::user()->id]);
        //dd($request->all());
        $user=User::find($id);
        if ($request->file('photo')){
            $user_image = $request->file('photo');

            $image_name = uniqid().'.'.$user_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/user');
            $user_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);

            if (file_exists('images/user/' .$user->image)){
                unlink('images/user/' .$user->image);
            }
        }
        if ($user->update($request->all())) {
            $request->session()->flash('success_message', 'User Updated Successfully');

        }else{
            $request->session()->flash('error_message','User Updated Failed');
        }
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $user=User::find($id);
        if ($user->delete()) {
            $request->session()->flash('success_message', 'User Deleted Successfully');

        }else{
            $request->session()->flash('error_message','User Deleted Failed');
        }
        return redirect()->route('user.index');
    }

    public function password(Request $request) {
        $this->validate($request, [
            'old_password'=> 'required',
            'new_password' =>'required|min:8',
            'confirm_password' => 'required|min:8|same:new_password',
            ]);
        $oldPassword = $request->old_password;
        $newPassword = $request->new_password;

        if(!Hash::check($oldPassword, Auth::user()->password)){
            $request->session()->flash('error_message','The specified password does not match the database password');
            return redirect()->back();
//            return back()->flash('erroe_message','The specified password does not match the database password'); //when user enter wrong password as current password
        }else{
            $request->user()->fill(['password' => Hash::make($newPassword)])->save(); //updating password into user table
            $request->session()->flash('success_message', 'Password has been updated');
//            return back()->flash('success_message','Password has been updated');

            return redirect()->back();
        }
//         echo 'here update query for password';
    }
    public function update1(Request $request)
    {
//        $data=$request->all();
        $request->request->add(['created_by'=>Auth::user()->id]);
        //dd($request->all());
        $user=User::find(Auth::user()->id);
        if ($request->file('photo')){
            $user_image = $request->file('photo');

            $image_name = uniqid().'.'.$user_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/user');
            $user_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);

            if (file_exists('images/user/' .$user->image)){
                unlink('images/user/' .$user->image);
            }
        }
        if ($user->update($request->all())) {
            $request->session()->flash('success_message', 'User Updated Successfully');

        }else{
            $request->session()->flash('error_message','User Updated Failed');
        }
        return redirect()->route('backend.profile');
    }

    public function profile()
    {
//        $data['user']=User::all();
        return view('backend.user.profile');
    }
}
